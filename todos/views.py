from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all
    context = {"lists": lists}
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {"details": details}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list = TodoListForm(request.POST, instance=list)
        if list.is_valid():
            list.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
    context = {"list": list, "form": form}
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/createtask.html", context)


def todo_item_update(request, id):
    task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        task = TodoItemForm(request.POST, instance=task)
        if task.is_valid():
            new_task = task.save()
            return redirect("todo_list_detail", id=new_task.list.id)
    else:
        form = TodoItemForm(instance=task)
    context = {"task": task, "form": form}
    return render(request, "todos/updatetask.html", context)
